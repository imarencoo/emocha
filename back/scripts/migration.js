/*eslint max-len: ["error", { "code": 100 }]*/

'use strict';

const mongoose = require('mongoose');
const config = require('../app/config');
const WidgetModel = require('../app/model/Widget');
const WidgetSizeModel = require('../app/model/WidgetSize');

async function startMigration() {
  await mongoose.connect(config.mongo.uri, config.mongo.options);

  const sizes = await getWidgetSizes();


  const url = 'https://fscl01.fonpit.de/userfiles/6970559/image/Nov15/Widget-Shazam-01-w782.jpg';
  const description = 'Lorem Ipsum is simply dummy text of the printing and typesetting.';

  await WidgetModel.insertMany([
    {
      name: 'Widget Prime',
      size: [sizes[0]._id, sizes[1]._id, sizes[2]._id],
      description: description,
      img: url,
    },
    {
      name: 'Widget Elite',
      size: [sizes[0]._id, sizes[1]._id],
      description: description,
      img: url,
    },
    {
      name: 'Widget Extreme Edition',
      size: [sizes[0]._id, sizes[1]._id],
      description: description,
      img: url,
    },
    {
      name: 'Widget Gold',
      size: [sizes[0]._id, sizes[1]._id, sizes[2]._id],
      description: description,
      img: url,
    },
    {
      name: 'Widget Platinum',
      size: [sizes[0]._id, sizes[1]._id, sizes[2]._id, sizes[3]._id],
      description: description,
      img: url,
    },
  ]
  );

  await mongoose.disconnect();
}

async function getWidgetSizes() {
  const sizes = await WidgetSizeModel.insertMany([
    {
      label: 'small',
    },
    {
      label: 'medium',
    },
    {
      label: 'large',
    },
    {
      label: 'x-large',
    },
  ]
  );

  return sizes;
}


startMigration();
