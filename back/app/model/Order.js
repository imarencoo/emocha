'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const order = require('../schema/Order');

const OrderSchema = new Schema(order);

module.exports = mongoose.model('Order', OrderSchema);
