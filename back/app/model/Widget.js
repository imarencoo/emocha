'use strict';

require('./WidgetSize');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const widget = require('../schema/Widget');

const WidgetSchema = new Schema(widget);

module.exports = mongoose.model('Widget', WidgetSchema);
