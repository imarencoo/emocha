'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const widgetSize = require('../schema/WidgetSize');

const WidgetSizeSchema = new Schema(widgetSize);

module.exports = mongoose.model('WidgetSize', WidgetSizeSchema);
