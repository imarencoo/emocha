'use strict';

const OrderModel = require('../model/Order');
const BaseController = require('./BaseController');

exports.create = async function(payload) {
  return await BaseController.create(OrderModel, payload);
};
