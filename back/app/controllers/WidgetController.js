'use strict';

const WidgetModel = require('../model/Widget');
const BaseController = require('./BaseController');

exports.find = async function(options = {}) {
  return await BaseController.list(WidgetModel, options);
};
