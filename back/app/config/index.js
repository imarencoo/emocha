'use strict';

module.exports = {
  mongo: {
    options: { useNewUrlParser: true },
    uri: process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/dev',
  },
};
