'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = {
  widgets: [
    {
      data: { type: Schema.Types.ObjectId, ref: 'Widget' },
      size: { type: Schema.Types.ObjectId, ref: 'WidgetSize' },
    },
  ],
};
