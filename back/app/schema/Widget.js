'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = {
  name: { type: String, required: true },
  description: { type: String, required: true },
  size: [{ type: Schema.Types.ObjectId, ref: 'WidgetSize' }],
  img: { type: String, required: true },
};
