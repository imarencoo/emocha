'use strict';

const httpResponse = require('../utils/http');
const contextManager = require('../utils/context');
const WidgetController = require('../app/controllers/WidgetController');
const errors = require('../utils/errors');

module.exports.find = async(event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const query = event.queryStringParameters || {};
  const filter = {};

  if (query.name) {
    filter.name = new RegExp(query.name, 'i');
  }

  try {
    await contextManager.handleContext();
    const brands = await WidgetController.find({
      page: query.page,
      limit: query.limit,
      filter: filter,
      populate: ['size'],
    });

    return httpResponse.success(brands);
  } catch (e) {
    return httpResponse.error(500, errors.UNEXPECTED);
  }
};
