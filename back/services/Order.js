'use strict';

const httpResponse = require('../utils/http');
const contextManager = require('../utils/context');
const OrderController = require('../app/controllers/OrderController');
const validator = require('../utils/validator');
const errors = require('../utils/errors');
const OrderSchema = require('../app/schema/Order');

module.exports.create = async(event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const body = JSON.parse(event.body || {});

  try {
    await validator.isValidJSONSchema(OrderSchema, body);
  } catch (e) {
    return httpResponse.error(400, errors.BAD_REQUEST);
  }

  try {
    await contextManager.handleContext();
    const category = await OrderController.create(body);
    return httpResponse.success(category);
  } catch (e) {
    return httpResponse.error(500, errors.UNEXPECTED);
  }
};
