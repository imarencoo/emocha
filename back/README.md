# Back

Build the architecture on serverless, because:
- Auto-scale each function separately and automatically.
- If a micro service falls, all the others keep running.
- You can use different providers (aws, azure, etc).

I thought that based on the requirements, in medium/large applications I use typescript with another structure (I mentioned this in the interview, separating them into domains by model, and sub folders like service, model, dto, controller, copying a java structure with springboot), but in this case, which is a small application, I prefer to simplify some structures so that the learning curve is easier.

## Getting started

### enviroment
You can set one enviroment variable:

- MONGODB_URI: uri of mongodb. default: localhost:27017/dev


### install and run
```
    npm install
    npm install -g serverless@1.39.1
    npm run script:migration
    npm run start
```
Starts on `http://localhost:3000`.

### I included several core libs

* Mongoose _-  mongodb driver.
* Serverless _- http framework.

And some **other useful libs** like eslint, serverless-offline, nodemon.

## Project structure

`services` 
In this folder are our http functions of serverless separated by each model (widget, order).

`app/controllers` 
The controllers are used to warpe logic related to a model, the idea of the controllers is that they can be used by any service and the logic is encapsulated. The idea is always to extend the BaseController because it has all the necessary functionality for the required operations (CRUD).

`app/model` 
the models are representations of mongo schema.

`app/schema` 
the schema are representations of schema structure.

`app/config` 
App configuration.

`utils/*` 
Useful snippets of code like schema validations, database connection, errors handler, http response, etc. I usually put it in a submodule of git or in a module of npm.

## Testing 
- Lint:  I use eslint based on strongloop rules. If you use vscode you can see the errors directly, or by console running ` npm run lint` .
- TDD: I usually use mocha and chai.

_Tested using NPM 6.2.0, Node v10.8.0, Git 2.19.0 over Mac OS 10.14.2.
