import RequestManager from './request';

const request = new RequestManager();

const API = {
};

API.url = `${API_URL}`;

API.getWidgets = function getCategories(name) {
  const queryParams = {
    limit: 15,
    page: 1,
  };

  if (name) { queryParams.name = name; }
  return request.get(`${API.url}/widget`, queryParams);
};

API.createOrder = function createOrder(body) {
  return request.post(`${API.url}/order`, {}, body);
};

export default API;
