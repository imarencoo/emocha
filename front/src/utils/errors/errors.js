import { put } from 'redux-saga/effects';

export const REDIRECT_ERROR = 'redirect';

function redirect(url) {
  window.location = `/#/${url}`;
}

function* handleError(actionType, error, props = {}) {
  if (error.__CANCEL__) {
    return;
  }

  const { message, ...otherProps } = props;
  yield put({ type: actionType, error: message || error.message, ...otherProps });

  if (error.response && error.response.status >= 500) {
    yield redirect('error/unexpected-error');
  }

  if (error.response && error.response.status === 404) {
    yield redirect('error/not-found');
  }

  if (error.response && error.response.status === 400) {
    yield redirect('error/forbidden');
  }
}

const SagasUtils = {
  handleError,
};

export default SagasUtils;
