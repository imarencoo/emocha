import { fork } from 'redux-saga/effects';
import widget from '../domain/widget/sagas';
import order from '../domain/order/sagas';

export default function* root() {
  yield fork(widget);
  yield fork(order);
}
