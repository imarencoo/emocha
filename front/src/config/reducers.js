import { widgets } from '../domain/widget/reducers';
import { orders } from '../domain/order/reducers';

export { widgets, orders };
