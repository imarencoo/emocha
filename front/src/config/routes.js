import { BrowserRouter, Route, Switch, HashRouter } from 'react-router-dom';
import React from 'react';
import { ListWidgetContainer } from '../pages/Widget';
import { PreOrderContainer, CreatedOrderContainer } from '../pages/Order';
import ErrorPage from '../pages/Error';

export default () => (
  <BrowserRouter>
    <HashRouter>
      <Switch>
        <Route exact path="/" component={ListWidgetContainer} />
        <Route exact path="/preview" component={PreOrderContainer} />
        <Route exact path="/finish" component={CreatedOrderContainer} />
        <Route exact path="/error/:code" component={ErrorPage} />
      </Switch>
    </HashRouter>
  </BrowserRouter>
);
