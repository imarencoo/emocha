import * as actionTypes from '../../action_types';

export const getWidgets = name => ({ type: actionTypes.GET_WIDGETS.EVENT, name });
