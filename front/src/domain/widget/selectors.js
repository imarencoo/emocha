
const getWidgets = state => state.widgets.list;
const WidgetSelectors = ({
  getWidgets,
});

export default WidgetSelectors;
