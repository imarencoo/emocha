import { takeEvery, call, put } from 'redux-saga/effects';
import API from '../../utils/api/api';
import * as actionTypes from '../../action_types';
import ErrorUtil from '../../utils/errors/errors';


function* getWidgets(action) {
  try {
    const widgets = yield call(API.getWidgets, action.name);
    yield put({
      type: actionTypes.GET_WIDGETS.SUCCESS,
      payload: { results: widgets.data.docs },
    });
  } catch (e) {
    ErrorUtil.handleError(actionTypes.GET_WIDGETS.FAILURE, e);
  }
}

export default function* root() {
  yield [
    yield takeEvery(actionTypes.GET_WIDGETS.EVENT, getWidgets),
  ];
}
