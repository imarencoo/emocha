import { combineReducers } from 'redux';
import * as ActionTypes from '../../action_types';
import { getActionName, DEFAULT_COLLECTION_STATE, genericAllActionTypeHandler, updateObject } from '../../utils/reducers';

function list(state = DEFAULT_COLLECTION_STATE, action) {
  switch (getActionName(action)) {
    case ActionTypes.GET_WIDGETS.NAME:
      return genericAllActionTypeHandler(state, action);
    default:
      return state;
  }
}

export const widgets = combineReducers({
  list,
});
