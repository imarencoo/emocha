import { takeEvery, call, put } from 'redux-saga/effects';
import API from '../../utils/api/api';
import * as actionTypes from '../../action_types';
import ErrorUtil from '../../utils/errors/errors';


function* createOrder(action) {
  try {
    const widgets = action.payload.payload.map((widget) => {
      return {
        data: widget._id,
        size: widget.selected,
      };
    });

    yield call(API.createOrder, { widgets });

    yield put({
      type: actionTypes.CREATE_ORDER.CLEAN,
    });

    action.push('/finish');
  } catch (e) {
    ErrorUtil.handleError(actionTypes.CREATE_ORDER.FAILURE, e);
  }
}

export default function* root() {
  yield [
    yield takeEvery(actionTypes.CREATE_ORDER.EVENT, createOrder),
  ];
}
