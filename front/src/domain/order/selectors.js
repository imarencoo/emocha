
const getOrder = state => state.orders.create;
const getAddedWidget = (state) => {
  const order = getOrder(state);
  return (order.payload || []).map(widget => widget._id);
};

const WidgetSelectors = ({
  getOrder,
  getAddedWidget,
});

export default WidgetSelectors;
