import * as actionTypes from '../../action_types';

export const addWidgetToOrder = widget =>
  ({ type: actionTypes.ADD_WIDGET_TO_ORDER.SUCCESS, payload: widget });

export const removeWidgetToOrder = widget =>
  ({ type: actionTypes.REMOVE_WIDGET_TO_ORDER.SUCCESS, payload: widget });


export const createOrder = (payload, push) =>
  ({ type: actionTypes.CREATE_ORDER.EVENT, payload, push });
