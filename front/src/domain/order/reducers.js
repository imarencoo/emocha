import { combineReducers } from 'redux';
import * as ActionTypes from '../../action_types';
import { getActionName, DEFAULT_COLLECTION_STATE, genericActionTypeHandler } from '../../utils/reducers';

function create(state = DEFAULT_COLLECTION_STATE, action) {
  switch (getActionName(action)) {
    case ActionTypes.ADD_WIDGET_TO_ORDER.NAME:
      return genericActionTypeHandler(state, { type: action.type, payload: (state.payload || []).concat(action.payload) });
    case ActionTypes.REMOVE_WIDGET_TO_ORDER.NAME:
      return genericActionTypeHandler(state, { type: action.type, payload: state.payload.filter(widget => action.payload._id !== widget._id) });
    case ActionTypes.CREATE_ORDER.NAME:
      return genericActionTypeHandler(state, action);
    default:
      return state;
  }
}

export const orders = combineReducers({
  create,
});
