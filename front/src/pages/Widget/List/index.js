import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import * as widgetActions from '../../../domain/widget/actions';
import * as orderActions from '../../../domain/order/actions';
import WidgetSelectors from '../../../domain/widget/selectors';
import OrderSelectors from '../../../domain/order/selectors';
import Layout from '../../../components/Layout';
import ListLayout from '../../../components/ListLayout';
import Card from '../../../components/Card';
import Searchbox from '../../../components/Input';
import WidgetProperties from '../../../components/WidgetProperties';
import EmptyPage from '../../../components/EmptyPage';
import ActionButton from '../../../components/ActionButton';
import AsideHeader from '../../../components/AsideHeader';

class WidgetListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { openProperties: false };
    this.changeSearch = debounce(this.changeSearch, 500);
  }

  componentDidMount() {
    this.props.actions.getWidgets();
  }

  changeSearch(value) {
    this.props.actions.getWidgets(value); 
  }

  render() {
    const classes = this.props.classes;
    
    return (
      <Layout page="order">
        <ListLayout
          headerLeft={<AsideHeader text="Select Widgets"  />}
          headerRight={<ActionButton disabled={this.props.addedWidgets.length === 0} text="preview" onClick={() => this.props.history.push('/preview')} />}
        >
          <div className={classes.body}>
            <Searchbox
              placeholder="Search Widgets"
              onChange={(e) => {
                this.changeSearch(e.target.value);
              }}
            />
            { this.props.list.payload.length === 0 && !this.props.list.isLoading && this.props.list.isReady && <EmptyPage
              icon={'😊'}
              title={'We couldn’t find anything.'}  
              description={'Adjust search for more results.'}
            />
            }
            {
              this.props.list.payload.length > 0 && 
              <div className={classes.list}>
                { this.props.list.payload.map(widget => 
                  (<Card
                    key={widget._id}
                    isAdded={this.props.addedWidgets.indexOf(widget._id) > -1}
                    removeWidget={() => this.props.actions.removeWidgetToOrder(widget)}
                    addWidget={newWidget => this.setState({ openProperties: true, widget: newWidget })}
                    widget={widget}
                  />))
                }
              </div>
            }
          </div>
        </ListLayout>
        { this.state.openProperties && <WidgetProperties
          widget={this.state.widget}
          handleClose={(widget) => {
            this.setState({ openProperties: false });
            if (widget) {
              this.props.actions.addWidgetToOrder(widget);
            }
          }}
          open={this.state.openProperties}
        />
        }
      </Layout>
    );
  }
}

WidgetListContainer.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  list: PropTypes.object,
  addedWidgets: PropTypes.arrayOf(PropTypes.string),
  history: PropTypes.object,
};

const mapStateToProps = state => ({
  list: WidgetSelectors.getWidgets(state),
  order: OrderSelectors.getOrder(state),
  addedWidgets: OrderSelectors.getAddedWidget(state),
});

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(Object.assign({}, orderActions, widgetActions), dispatch) });

const styles = theme => ({
  body: {
    padding: theme.spacing.unit * 4,
    height: '100%',
  },
  list: {
    display: 'grid',
    overflow: 'auto',
    height: '100%',
    'grid-template-columns': 'auto auto auto',
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(WidgetListContainer));
