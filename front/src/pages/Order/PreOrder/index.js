import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import * as widgetActions from '../../../domain/widget/actions';
import * as orderActions from '../../../domain/order/actions';
import WidgetSelectors from '../../../domain/widget/selectors';
import OrderSelectors from '../../../domain/order/selectors';
import AsideHeader from '../../../components/AsideHeader';
import Layout from '../../../components/Layout';
import ListLayout from '../../../components/ListLayout';
import Card from '../../../components/Card';
import ActionButton from '../../../components/ActionButton';

class WidgetListContainer extends React.Component {
  componentDidMount() {
    if (!this.props.order.payload || this.props.order.payload.length === 0) {
      this.props.history.push('/');
    }
  }

  render() {
    const classes = this.props.classes;
    return (
      <Layout page="order">
        <ListLayout
          headerLeft={<AsideHeader text="Preview" action={() => this.props.history.push('/')} enableButton />}
          headerRight={<ActionButton text="create" onClick={() => this.props.actions.createOrder(this.props.order, this.props.history.push)} />}
        >
          <div className={classes.body}>
            <div className={classes.list}>
              {(this.props.order.payload || []).map(widget => <Card key={widget._id} disableAction widget={widget} />) }
            </div>
          </div>
        </ListLayout>
      </Layout>
    );
  }
}

WidgetListContainer.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  history: PropTypes.object,
  order: PropTypes.object,
};

const mapStateToProps = state => ({
  list: WidgetSelectors.getWidgets(state),
  order: OrderSelectors.getOrder(state),
  addedWidgets: OrderSelectors.getAddedWidget(state),
});

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(Object.assign({}, orderActions, widgetActions), dispatch) });

const styles = theme => ({
  body: {
    padding: theme.spacing.unit * 4,
    height: '100%',
  },
  list: {
    display: 'grid',
    overflow: 'auto',
    height: '100%',
    'grid-template-columns': 'auto auto auto',
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(WidgetListContainer));