import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Buttom from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import Layout from '../../../components/Layout';

class WidgetListContainer extends React.PureComponent {
  render() {
    const classes = this.props.classes;
    return (
      <Layout page="order">
        <div className={classes.container}>
          <Typography
            classes={{ root: this.props.classes.icon }}
            variant="display4"
            className={classes.text}
          >
            <span>😎</span>
          </Typography>
          <Typography
            gutterBottom
            variant="headline"
            className={classes.text}
          >
            Success!
          </Typography>
          <Typography
            variant="subheading"
            gutterBottom
            className={classes.text}
          >
            The order has been created.
          </Typography>

          <Buttom variant="contained" onClick={() => this.props.history.push('/')} color="primary">Back to Creation</Buttom>
        </div>
      </Layout>
    );
  }
}

WidgetListContainer.propTypes = {
  history: PropTypes.object,
  classes: PropTypes.object,
};

const styles = () => ({
  icon: {
    color: 'inherit',
  },
  container: {
    margin: '0 auto',
    display: 'flex',
    'align-items': 'center',
    'justify-content': 'center',
    'flex-direction': 'column',
  },
});


export default withStyles(styles)(WidgetListContainer);
