import React from 'react';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import PropTypes from 'prop-types';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import CardContent from '@material-ui/core/CardContent';
import { withStyles } from '@material-ui/core/styles';


const CardItem = ({ classes, widget, addWidget, removeWidget, isAdded, disableAction }) => (
  <Card className={classes.card}>
    <CardActionArea>
      <CardMedia
        className={classes.media}
        image={widget.img}
        title="Contemplative Reptile"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          { widget.name }
        </Typography>
        <Typography variant="body2" color="textSecondary" noWrap component="p">
          { widget.description }
        </Typography>
      </CardContent>
    </CardActionArea>
    <CardActions>
      { !disableAction &&
      <Button onClick={() => isAdded ? removeWidget(widget) : addWidget(widget)} size="small" color="primary">
        { isAdded ? 'Remove' : 'Add' }
      </Button>
      }
    </CardActions>
  </Card>
);

CardItem.propTypes = {
  disableAction: PropTypes.bool,
  isAdded: PropTypes.bool,
  removeWidget: PropTypes.func,
  addWidget: PropTypes.func,
  widget: PropTypes.object,
  classes: PropTypes.object,
};


const styles = () => ({
  card: {
    width: 345,
    height: 281,
    margin: '15px auto',
  },
  media: {
    height: 140,
  },
});

export default withStyles(styles)(CardItem);
