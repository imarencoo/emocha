import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

const AsideHeader = ({ classes, enableButton, action, text }) => (
  <div className={classes.container}>
    {
      enableButton &&
      <IconButton onClick={action}>
        <ArrowBack />
      </IconButton>
    }
    <Typography variant="h6">{text}</Typography>
  </div>
);


AsideHeader.propTypes = {
  text: PropTypes.string,
  action: PropTypes.func,
  enableButton: PropTypes.bool,
  classes: PropTypes.object,
};


AsideHeader.defaultProps = {
  enableButton: false,
};

const styles = () => ({
  container: {
    display: 'inline-flex',
    'align-items': 'center',
  },
});

export default withStyles(styles)(AsideHeader);