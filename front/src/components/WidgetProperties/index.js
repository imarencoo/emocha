import React from 'react';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class WidgetPropertiesDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = { size: props.widget.size[0]._id };
  }

  render() {
    return (
      <Dialog open onClose={() => this.props.handleClose()} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Properties</DialogTitle>
        <DialogContent>
          <DialogContentText>
              To add a widget, please enter valid size here.
          </DialogContentText>
          <Select
            value={this.state.size}
            onChange={event => this.setState({ size: event.target.value })}
          >
            {this.props.widget.size.map(size => <MenuItem key={size._id} value={size._id}>{size.label}</MenuItem>)}
          </Select>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => this.props.handleClose()} color="primary">
              Cancel
          </Button>
          <Button onClick={() => this.props.handleClose(Object.assign({}, this.props.widget, { selected: this.state.size }))} color="primary">
            Finish
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

WidgetPropertiesDialog.propTypes = {
  handleClose: PropTypes.func,
  widget: PropTypes.object,
};

export default WidgetPropertiesDialog;