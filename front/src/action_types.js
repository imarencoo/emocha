function createBaseRequestActionType(actionName) {
  if (actionName === '') {
    return {
      NAME: 'BASE_ACTION_TYPE',
      EVENT: 'EVENT',
      SUCCESS: 'SUCCESS',
      FAILURE: 'FAILURE',
      LOADING: 'LOADING',
      CLEAN: 'CLEAN',
    };
  }
  return {
    NAME: actionName,
    EVENT: `${actionName}_EVENT`,
    SUCCESS: `${actionName}_SUCCESS`,
    FAILURE: `${actionName}_FAILURE`,
    LOADING: `${actionName}_LOADING`,
    CLEAN: `${actionName}_CLEAN`,
  };
}


export const BASE_ACTION = createBaseRequestActionType('');
export const BASE_ACTION_NEXT_PAGE = createBaseRequestActionType('');
export const BASE_ACTION_LIST_SEARCH = createBaseRequestActionType('');

// widget
export const GET_WIDGETS = createBaseRequestActionType('GET_WIDGETS');

// order
export const ADD_WIDGET_TO_ORDER = createBaseRequestActionType('ADD_WIDGET_TO_ORDER');
export const REMOVE_WIDGET_TO_ORDER = createBaseRequestActionType('REMOVE_WIDGET_TO_ORDER');
export const CREATE_ORDER = createBaseRequestActionType('CREATE_ORDER');